import {Row, Col} from 'react-bootstrap'
import {Link} from 'react-router-dom';

export default function NotFound(){
	return (
		<Row>
			<Col className = "col-4 mx-auto">
				<h1 className ="text-center">Page Not Found</h1>
				<p className = "text-center"> Go back to <Link to = '/'>homepage</Link>.</p>
			</Col>
		</Row>
	)
}