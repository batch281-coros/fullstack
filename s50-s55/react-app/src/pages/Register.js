// import Button from 'react-bootstrap/Button';
// import Form from 'react-bootstrap/Form';

import { Button, Form, Row, Col } from 'react-bootstrap'
// We need to import the useState from react
import { useState, useEffect, useContext } from 'react';

import {Navigate, useNavigate} from 'react-router-dom';

import UserContext from '../UserContext';

import Swal2 from 'sweetalert2';

export default function Register(){

	const navigate = useNavigate();

	const {user} = useContext(UserContext);

	// State hooks to store the values of the input fields

	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');

	const [isPassed, setIsPassed] = useState(true);

	const [isDisabled, setIsDisabled] = useState(true);

	// We are going to add/create a state that will declare whether the password1 and password2 is equal
	const [isPasswordMatch, setIsPasswordMatch] = useState(true)

	// When the email changes it will have a side effect that  will console its value
	useEffect(() => {
		if(email.length >  15){
			setIsPassed(false);
		} else {
			setIsPassed(true);
		}
	}, [email]);

	// This useEffect will disable or enable our sign up button
	useEffect(() => {
		// We are going to add if statement and all the condition that we mention should be satisfied before we enable the sign up button
		if( email !== '' && password1 !== '' && password2 !== '' && password1 === password2 && email.length <= 15 && firstName !== '' && lastName !== '' && mobileNo.length >= 11 && mobileNo !== ''){
			setIsDisabled(false);
		} else {
			setIsDisabled(true);
		}
	}, [email, password1, password2, firstName, lastName, mobileNo ]);

	// Function to simulate user registration
	function registerUser(event){
		// Prevent page reloading
		event.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(response => response.json())
		.then(data => {

			if(data === true){
				Swal2.fire({
					title: 'Duplicate email found',
					icon: 'error',
					text: 'Please provide a different email.'
				})
			} else {
				// alert('Thank you for registering!');

				fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						mobileNo: mobileNo,
						password: password1,
					})
				})
				.then(response => response.json())
				.then(data => {

					if(data === false){
						Swal2.fire({
					title: 'Registration Unsuccessful',
					icon: 'error',
					text: 'Please try again'
				})
					} else {
						Swal2.fire({
					title: 'Registration Successful',
					icon: 'success',
					text: 'Welcome to Zuitt!'
				})

						navigate('/login')
					}
				})
			}
		})

		

		
		setFirstName('');
		setLastName('');
		setMobileNo('');
		setEmail('');
		setPassword1('');
		setPassword2('');
	}

	// useEffect to validate whether the password1 is equal to password2

	useEffect(() => {

		if(password1 === password2){
			setIsPasswordMatch(true);
		} else {
			setIsPasswordMatch(false);
		} 
	}, [password1, password2])



	return (
		user.id === null || user.id === undefined
		?
			<Row>
				<Col className = "col-6 mx-auto" >
					<h1>Register</h1>
					<Form onSubmit = {event => registerUser(event)}>

						<Form.Group className="mb-3" controlId="formBasicEmail">
							<Form.Label>First Name</Form.Label>
							<Form.Control 
							type="text" 
							placeholder="Enter first name"
							value = {firstName}
							onChange = {(event) => setFirstName(event.target.value)}
							/>
						</Form.Group>

						<Form.Group className="mb-3" controlId="formBasicEmail">
							<Form.Label>Last Name</Form.Label>
							<Form.Control 
							type="text" 
							placeholder="Enter last name"
							value = {lastName}
							onChange = {(event) => setLastName(event.target.value)}
							/>
						</Form.Group>

					    <Form.Group className="mb-3" controlId="formBasicEmail">
					    	<Form.Label>Email address</Form.Label>
					    	<Form.Control 
					    	type="email" 
					    	placeholder="Enter email"
					    	value = {email}
					    	onChange = {(event) => setEmail(event.target.value)}
					    	/>
					    	<Form.Text className="text-muted" hidden={isPassed}>
					          The email should not exceed 15 characters
					    	</Form.Text>
					    </Form.Group>

					    <Form.Group className="mb-3" controlId="formBasicEmail">
					    	<Form.Label>Mobile Number</Form.Label>
					    	<Form.Control 
					    	type="text" 
					    	placeholder="Enter mobile number"
					    	value = {mobileNo}
							onChange = {(event) => setMobileNo(event.target.value)}
					    	/>
					    </Form.Group>

					    <Form.Group className="mb-3" controlId="formBasicPassword1">
					       <Form.Label>Password</Form.Label>
					       <Form.Control 
					       type="password" 
					       placeholder="Password"
					       value = {password1}
					       onChange = {event => setPassword1(event.target.value)}
					       />
					    </Form.Group>

					    <Form.Group className="mb-3" controlId="formBasicPassword2">
					        <Form.Label>Verify Password</Form.Label>
					        <Form.Control 
					        type="password" 
					        placeholder="Retype your nominated password" 
					        value = {password2}
					        onChange = { event => setPassword2(event.target.value)}
					        />
					        <Form.Text className="text-danger" hidden={isPasswordMatch}>
					              The password does not match!
					        </Form.Text>
					     </Form.Group>

					     <Button variant="primary" type="submit" disabled = {isDisabled}>
					        Sign Up
					     </Button>
					</Form>
				</Col>
			</Row>
		:

		<Navigate to = '*' />
	);
};