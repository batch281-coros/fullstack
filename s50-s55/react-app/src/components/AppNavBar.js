import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';

import { useContext } from 'react';
import {NavLink, Link} from 'react-router-dom';

import UserContext from '../UserContext';

// The as keyword allows components to be treated as if they are different components gaining access to it's properties and functionalities
// The to keyword is used in place of the 'href' for providing the url for the page 
export default function AppNavBar() {

	const {user} = useContext(UserContext);
	// In here, I will capture the value from our localStorage and then store it in a state.
	// Syntax:
		// localStorage.getItem('key/property')

	// console.log(localStorage.getItem('email'));

	// const [user, setUser] = useState(localStorage.getItem('email'));

/*	useEffect(() => {
		setUser(localStorage.getItem('email'))
	}, [localStorage.getItem('email')])*/

	return (

		<Navbar bg="primary" expand="lg">
		   	<Container fluid>
		       	<Navbar.Brand as = {Link} to = '/'>Zuitt Booking</Navbar.Brand>
		       	<Navbar.Toggle aria-controls="basic-navbar-nav"/>
		       	<Navbar.Collapse id="basic-navbar-nav">
		           	<Nav className="ms-auto">
		           	<Nav.Link as = {NavLink} to = '/'>Home</Nav.Link>
		           	<Nav.Link as = {NavLink} to = '/courses'>Courses</Nav.Link>


		           	{
		           		user.id === null || user.id === undefined
		           		?
		           		<>
		           			<Nav.Link as = {NavLink} to = '/login'>Login</Nav.Link>
		           			<Nav.Link as = {NavLink} to = '/register'>Register</Nav.Link>
		           		</>
		           		
		           		:
		           		<Nav.Link as = {NavLink} to = '/logout'>Logout</Nav.Link>
		           	}

		           	</Nav>
		       	</Navbar.Collapse>
		   	</Container>
		</Navbar>
	)
}