// import { Fragment } from 'react';
import { Container } from 'react-bootstrap';
import AppNavBar from './components/AppNavBar';
// import Banner from './components/Banner';
// import Highlights from './components/Highlights'
import Courses from './pages/Courses';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import NotFound from './pages/NotFound';
import CourseView from './components/CourseView';

import {useState, useEffect} from 'react';

// import the UserProvider from the UserContext to provide the content of value of our UserContext
import { UserProvider } from './UserContext';

import './App.css';

// Import neccessary modules from react-router that will be used for the routing
import { BrowserRouter, Route, Routes} from 'react-router-dom';

// React JS is a SPA
// whenever a link is clicked, it functions as if the page is being reloaded but what actually happens is it goes through the process of rendering, mounting, rerendering and remounting.

// The 'BrowserRouter' component will enable us to simulate page navigation by synchronizing the show content and the shown url in the web browser

// 'Routes' component holds all our Route component. It selects which 'Route' component to show based on the URDL endpoint. For example. when '/courses' is visited in the web browser React.js will show the Courses component to us

function App() {

  // State hook for the user state that's defined here for a global scoping
  // Initialized as the value of the user information from brower's localStorage
  // This will be used to store the user info. and will be used for validating if a user is logged in on the app or not
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  // This function is for clearing localStorage on logout
  const unsetUser = () => {
    localStorage.clear();
  };

  // useEffect(() => {
  //   console.log(user);
  // }, [user]);

  // Para di mawala kapag nag refresh habang nakalogin
  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(response => response.json())
    .then(data => {
      setUser({
        id: data._id,
        isAdmin: data.isAdmin
      })
    })
  }, [])












  return (
    <UserProvider value = {{user, setUser, unsetUser}}>
      <BrowserRouter>
        <AppNavBar />
        <Container>
            <Routes>
                <Route path ='/' element = {<Home/>} />
                <Route path ='/courses' element = {<Courses/>} />
                <Route path ='/register' element = {<Register/>} />
                <Route path ='/login' element = {<Login/>} />
                <Route path ='/logout' element = {<Logout/>} />
                <Route path ='/courses/:id' element = {<CourseView />} />
                <Route path ='*'element = {<NotFound />} />
            </Routes>
        </Container>
      </BrowserRouter>
    </UserProvider>
  );
}

export default App;
