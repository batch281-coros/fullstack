const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

const updateName =  () => {
	spanFullName.innerHTML = (`${txtFirstName.value} ${txtLastName.value}`)
};

txtFirstName.addEventListener("keyup",  updateName);

txtLastName.addEventListener("keyup",  updateName);



/*txtFirstName.addEventListener("keyup",(event) => {
	console.log(event.target);
	console.log(event.target.value);
});*/


