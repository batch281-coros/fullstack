let collection = [];

// Write the queue functions below.

function print() {
    // It will show the array

    return collection;
}

function enqueue(element) {
    //In this function you are going to make an algo that will add an element to the array (last element)
    collection[collection.length] = element
   return collection
}

function dequeue() {
    // In here you are going to remove the first element in the array

    for (let i = 0; i < collection.length - 1; i++) {
      collection[i] = collection[i + 1];
    }

    collection.length = collection.length - 1;

    return collection

}

function front() {
    // you will get the first element
        let firstElement = collection[0]
    return firstElement
}


// starting from here, we cannot use .length property

function size() {
     // Number of elements 

    let numberOfElements = 0;
    let index = 0;

    while(collection[index] !== null && collection[index] !== undefined ){
        index++;
        numberOfElements++;
    }

    return numberOfElements;


}

function isEmpty() {
    //it will check whether the function is empty or not

    if (collection[0] !== null){
        return false
    } else {
        return true
    }
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};